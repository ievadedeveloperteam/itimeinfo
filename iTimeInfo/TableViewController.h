//
//  TableViewController.h
//  iTimeInfo
//
//  Created by Hao He on 13-2-15.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AddViewController.h"
#import "iTime.h"
#import "iAUnit.h"

@interface TableViewController : UITableViewController<CLLocationManagerDelegate,AddViewControllerDelegate>
{
    NSTimer *updateTimer;
    CLLocationManager *locationManager;
    CLLocation * currentLocation;
    NSMutableArray *displayedUnits;
    NSMutableArray *unusedUnits;
}


@property(nonatomic,retain) NSTimer* updateTimer;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain)CLLocation * currentLocation;
@property (nonatomic, retain)NSMutableArray *displayedUnits;
@property (nonatomic, retain)NSMutableArray *unusedUnits;

-(void)update;
-(void)addItems;
-(void)addItems:(NSArray*)unitsToAdd;
-(NSMutableArray*)mergeSort:(NSMutableArray*)list;
-(NSMutableArray*)mergeLeft:(NSMutableArray*)left Right:(NSMutableArray*)right;
@end
