//
//  AppDelegate.h
//  iTimeInfo
//
//  Created by Hao He on 13-2-15.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
	UIWindow *                  _window;
	UINavigationController *    _navController;
    UITableViewController*      _tableController;
}

@property (nonatomic, retain) IBOutlet UIWindow *               window;
@property (nonatomic, retain) IBOutlet UINavigationController * navController;
@property (nonatomic, retain) IBOutlet UITableViewController*   tableController;

@end
