//
//  TableViewController.m
//  iTimeInfo
//
//  Created by Hao He on 13-2-15.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

//#import <CoreLocation/CoreLocation.h>
#import "TableViewController.h"
#import "AddViewController.h"
#import "iTime.h"
#import "iAUnit.h"

@interface TableViewController ()

@end

@implementation TableViewController


@synthesize updateTimer=_updateTimer;
@synthesize locationManager=_locationMAnager;
@synthesize currentLocation=_currentLocation;
@synthesize displayedUnits=_displayedUnits;
@synthesize unusedUnits=_unusedUnits;

- (id)init
{
    self=[super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
        self.displayedUnits=[[NSMutableArray alloc] init];
        /*for(iAUnit *aUnit in [iATime allUnits]){
            
            if(aUnit.order>=0){
                [self.displayedUnits addObject:aUnit];
                
            }
        }*/
        self.displayedUnits=[self mergeSort:self.displayedUnits];

        self.title=@"iTimeInfo";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStyleBordered target:self action:@selector(addItems)];
    
    self.locationManager=[[CLLocationManager alloc] init];
    self.locationManager.delegate=self;
    self.locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    //[self performSelector:@selector(stopUpdatingLocation:) withObject:@"Timed Out" afterDelay:10.0];

    //NSArray *allUnits=iTime.allUnits;
    for(iAUnit *aUnit in [iTime allUnits]){
        
        if(aUnit.order>=0){
            [self.displayedUnits addObject:aUnit];
            
        }
    }
    
    //NSLog([NSString stringWithFormat:@"%d",[self.displayedUnits count]]);
    [self.tableView reloadData];
    self.updateTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(update) userInfo:nil repeats:YES];

}

-(NSMutableArray*)mergeSort:(NSMutableArray*)list{
    //NSLog([NSString stringWithFormat:@"%d",[list count]]);
    if([list count]<=1){
        return list;
    }
    NSMutableArray *left=[[NSMutableArray alloc] init];
    NSMutableArray *right=[[NSMutableArray alloc] init];
    int middle=[list count]/2;
    for(int i=0;i<middle;i++){
        [left addObject:[list objectAtIndex:i]];
    }
    for(int j=middle;j<[list count];j++){
        [right addObject:[list objectAtIndex:j]];
    }
    left=[self mergeSort:left];
    right=[self mergeSort:right];
    //NSLog([NSString stringWithFormat:@"%d",[left count]]);
    return [self mergeLeft:left Right:right];
}

-(NSMutableArray*)mergeLeft:(NSMutableArray*)left Right:(NSMutableArray*)right{
    NSMutableArray *temp=[[NSMutableArray alloc] init];
    while([left count]>0&&[right count]>0){
        if([(iAUnit*)[left objectAtIndex:0] order]<=[(iAUnit*)[right objectAtIndex:0] order]){
            [temp addObject:[left objectAtIndex:0]];
            [left removeObjectAtIndex:0];
        }else{
            [temp addObject:[right objectAtIndex:0]];
            [right removeObjectAtIndex:0];
        }
    }
    if([left count]>0){
        [temp addObjectsFromArray:left];
    }
    if([right count]>0){
        [temp addObjectsFromArray:right];
    }
    return temp;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [self.displayedUnits count];
}

-(NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    return @"App developed by Luke";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if(cell==nil){
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    //NSLog([NSString stringWithFormat:@"%d",[self.displayedUnits count]]);
    iAUnit *aUnit=[self.displayedUnits objectAtIndex:indexPath.row];
    cell.textLabel.text=aUnit.title;
    cell.detailTextLabel.text=aUnit.value;
    
    return cell;
}

-(void)update{
    if(!self.editing){
        [iTime updateWithLocation:currentLocation];
        NSEnumerator *enumerator=[self.displayedUnits objectEnumerator];
        for(UITableViewCell* cell in [(UITableView*)self.view visibleCells]){
            cell.detailTextLabel.text=((iAUnit*)[enumerator nextObject]).value;
        }
    }
}



/*
 * We want to get and store a location measurement that meets the desired accuracy. For this example, we are
 *      going to use horizontal accuracy as the deciding factor. In other cases, you may wish to use vertical
 *      accuracy, or both together.
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    // test the age of the location measurement to determine if the measurement is cached
    // in most cases you will not want to rely on cached measurements
    NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 5.0) return;
    // test that the horizontal accuracy does not indicate an invalid measurement
    if (newLocation.horizontalAccuracy < 0) return;
    // test the measurement to see if it is more accurate than the previous measurement
    if (currentLocation == nil || currentLocation.horizontalAccuracy > newLocation.horizontalAccuracy) {
        // store the location as the "best effort"
        currentLocation = newLocation;
        // test the measurement to see if it meets the desired accuracy
        //
        // IMPORTANT!!! kCLLocationAccuracyBest should not be used for comparison with location coordinate or altitidue
        // accuracy because it is a negative value. Instead, compare against some predetermined "real" measure of
        // acceptable accuracy, or depend on the timeout to stop updating. This sample depends on the timeout.
        //
        if (newLocation.horizontalAccuracy <= locationManager.desiredAccuracy) {
            // we have a measurement that meets our requirements, so we can stop updating the location
            //
            // IMPORTANT!!! Minimize power usage by stopping the location manager as soon as possible.
            //
            [locationManager stopUpdatingLocation];
            locationManager.delegate = nil;
            // we can also cancel our previous performSelector:withObject:afterDelay: - it's no longer necessary
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopUpdatingLocation:) object:nil];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    // The location "unknown" error simply means the manager is currently unable to get the location.
    // We can ignore this error for the scenario of getting a single location fix, because we already have a
    // timeout that will stop the location manager to save power.
    if ([error code] != kCLErrorLocationUnknown) {
        [locationManager stopUpdatingLocation];
        locationManager.delegate = nil;
    }
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
       return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        //NSLog([NSString stringWithFormat:@"%d",[indexPath row]]);
        //NSLog([NSString stringWithFormat:@"%@",((iAUnit*)([self.displayedUnits objectAtIndex:[indexPath row]])).value]);
        iAUnit *unitToDelete=[self.displayedUnits objectAtIndex:[indexPath row]];
        //NSLog([NSString stringWithFormat:@"%@",unitToDelete.value]);
        //NSLog([NSString stringWithFormat:@"%d",[indexPath row]]);
        //NSLog([NSString stringWithFormat:@"%@",((iAUnit*)([self.displayedUnits objectAtIndex:[indexPath row]])).value]);
        unitToDelete.order=-1;
        [[NSUserDefaults standardUserDefaults]setInteger:-1 forKey:unitToDelete.name];
        [self.displayedUnits removeObjectAtIndex: [indexPath row]];
        for(int i=0;i<self.displayedUnits.count;i++){
            ((iAUnit*)[self.displayedUnits objectAtIndex:i]).order=i;
            [[NSUserDefaults standardUserDefaults]setInteger:i forKey:((iAUnit*)[self.displayedUnits objectAtIndex:i]).name];
        }
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        
    }
}

//add item button function
-(void)addItems{
    self.unusedUnits=[[NSMutableArray alloc]init];
    for(iAUnit *aUnit in iTime.allUnits){
        if(aUnit.order<0){
            [self.unusedUnits addObject:aUnit];
        }
    }
    AddViewController *newAddViewController=[[AddViewController alloc] init];
    newAddViewController.delegate=self;
    [self.navigationController pushViewController:newAddViewController animated:YES];
    newAddViewController=nil;
}

-(void)addItems:(NSArray*)unitsToAdd{
    [self.displayedUnits addObjectsFromArray:unitsToAdd];
    for(int i=0;i<self.displayedUnits.count;i++){
        ((iAUnit*)[self.displayedUnits objectAtIndex:i]).order=i;
        [[NSUserDefaults standardUserDefaults]setInteger:i forKey:((iAUnit*)[self.displayedUnits objectAtIndex:i]).name];
        [self.tableView reloadData];
        //[self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:[self.displayedUnits count]-1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    }
    
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    // DRM: we want to perform the actions from this block in the main thread, but
    // asynchronously to avoid excessive delays which were causing issues.
    //
    dispatch_async(dispatch_get_main_queue(), ^void()
                   {
                       if (self.tableView.editing == false) {
                           if(self.updateTimer==nil){
                               self.updateTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(update) userInfo:nil repeats:YES];
                           }
                       }else {
                           if(self.updateTimer){
                               [self.updateTimer invalidate];
                               self.updateTimer=nil;
                           }
                       }
                   });
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    iAUnit* unitToRearrange=[self.displayedUnits objectAtIndex:[fromIndexPath row]];
    [self.displayedUnits removeObjectAtIndex:[fromIndexPath row]];
    [self.displayedUnits insertObject:unitToRearrange atIndex:[toIndexPath row]];
    for(int i=0;i<self.displayedUnits.count;i++){
        ((iAUnit*)[self.displayedUnits objectAtIndex:i]).order=i;
        [[NSUserDefaults standardUserDefaults]setInteger:i forKey:((iAUnit*)[self.displayedUnits objectAtIndex:i]).name];
    }
    //NSLog(((iAUnit*)[displayedUnits objectAtIndex:0]).title);
    [self.tableView reloadData];
}


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
