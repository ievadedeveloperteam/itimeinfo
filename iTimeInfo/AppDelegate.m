//
//  AppDelegate.m
//  iTimeInfo
//
//  Created by Hao He on 13-2-15.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import "AppDelegate.h"
#import "TableViewController.h"
#import "iTime.h"

@implementation AppDelegate


@synthesize window        = _window;
@synthesize navController = _navController;
@synthesize tableController=_tableController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    //#pragma unused(application)
   
    
    //assert(self.window != nil);
    //assert(self.navController != nil);
    self.tableController=[[TableViewController alloc] init];
    //assert(vc != nil);
    self.navController=[[UINavigationController alloc] init];
    
    
    self.window.rootViewController=self.navController;
    [self.window addSubview:self.navController.view];
    [self.navController pushViewController:self.tableController animated:NO];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"notFirstTime"]){
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"notFirstTime"];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"localDate"];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"localTime"];
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"coordinatedUniversalTime"];
        [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"julianDay"];
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"locationCoordinate"];
        [[NSUserDefaults standardUserDefaults] setInteger:5 forKey:@"localMeanSolarTime"];
        [[NSUserDefaults standardUserDefaults] setInteger:6 forKey:@"localApparentSolarTime"];
        [[NSUserDefaults standardUserDefaults] setInteger:7 forKey:@"localMeanSiderealTime"];
        [[NSUserDefaults standardUserDefaults] setInteger:8 forKey:@"localApparentSiderealTime"];
    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
