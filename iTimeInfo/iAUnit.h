//
//  iAUnit.h
//  iAstronomy
//
//  Created by Hao He on 13-2-8.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iAUnit : NSObject{
    //order in display list. -1 for disabled
    NSInteger order;
    NSString* title;
    NSString* value;
    NSString* name;
}

@property(readwrite) NSInteger order;
@property(nonatomic,retain) NSString* title;
@property(nonatomic, retain) NSString* value;
@property(nonatomic, retain) NSString* name;

-(id)initWithTitle:(NSString*)t Value:(NSString*)v Name:(NSString*)n;

@end
