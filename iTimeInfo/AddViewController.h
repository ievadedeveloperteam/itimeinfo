//
//  AddViewController.h
//  iTimeInfo
//
//  Created by Hao He on 13-2-15.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddViewControllerDelegate <NSObject>

@required
-(void)addItems:(NSArray*)unitsToAdd;
-(NSMutableArray*)unusedUnits;

@end

@interface AddViewController : UITableViewController{
    id<AddViewControllerDelegate> delegate;
}

@property(nonatomic,assign,readwrite) id<AddViewControllerDelegate> delegate;

-(void)back;

@end

