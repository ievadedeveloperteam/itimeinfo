//
//  iTime.h
//  iTimeInfo
//
//  Created by Hao He on 13-2-15.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "TableViewController.h"

@interface iTime : NSObject


+(NSArray*)allUnits;

+(void)updateWithLocation:(CLLocation*)currentLocation;

@end
