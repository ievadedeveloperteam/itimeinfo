//
//  iAUnit.m
//  iAstronomy
//
//  Created by Hao He on 13-2-8.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import "iAUnit.h"

@implementation iAUnit

@synthesize order;
@synthesize title;
@synthesize value;
@synthesize name;

-(id)initWithTitle:(NSString*)t Value:(NSString*)v Name:(NSString *)n{
    if(self=[super init]){
        self.order=[[NSUserDefaults standardUserDefaults] integerForKey:n];
        //NSLog([NSString stringWithFormat:@"%d",self.order]);
        self.title=t;
        self.value=v;
        self.name=n;
    }
    return self;
}

@end
